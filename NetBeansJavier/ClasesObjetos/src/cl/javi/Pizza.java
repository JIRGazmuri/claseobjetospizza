/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.javi;

/**
 *
 * @author Javier Rivas
 */
public class Pizza {
    //01 Crear atributos
    String nombre,
           tamano,
           masa;
    
    //02 Construir metodos (customer)
    
    public void preparar(){
        System.out.println("¡Estamos preparando tu pizza!\n");
    }
    public void calentar(){
        System.out.println("¡Estamos calentando tu pizza!\n");
    }
    //04 crear constructor con y sin parámetro

    public Pizza() {
    }

    public Pizza(String nombre, String tamano, String masa) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.masa = masa;
    }
    
    //05 Crear metodos setter y getter

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    
    // Crear método toString

    @Override
    public String toString() {
        return "Pizza{" + "nombre=" + nombre + ", tamano=" + tamano + ", masa=" + masa + '}';
    }
    
}
