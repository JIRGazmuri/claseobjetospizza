/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.javi;

/**
 *
 * @author Javier Rivas
 */
public class Pizzeria {
    
    public static void main(String[] args) {
        //03 instanciar objetos

        //Pizza pizza1 = new Pizza();
        //pizza1.nombre="Española";
        //System.out.println(pizza1.nombre);
        
        Pizza pizza1 = new Pizza();
        pizza1.getNombre();
        System.out.println(pizza1.getNombre());
        pizza1.setNombre("Española");
        System.out.println(pizza1.getNombre());
        
        Pizza pizza2 = new Pizza("Pepperoni", "Familiar", "Delgada");
        pizza2.setNombre("Vegetariana");
        System.out.println(pizza2.toString());
        
        Pizza pizza3 = new Pizza("Vegana", "Extra Grande", "Borde queso vegano");      
        System.out.println("Nombre pizza: \n"+pizza3.getNombre());
        System.out.println("Tamaño pizza: \n"+pizza3.getTamano());
        System.out.println("Masa pizza: \n"+pizza3.getMasa()+"\n");
        pizza3.preparar();
        pizza3.calentar();
        System.out.println(pizza3.toString());
        
    }
    
    
    
}
